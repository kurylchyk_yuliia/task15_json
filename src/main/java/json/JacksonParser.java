package json;
import com.kurylchyk.Candy;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
    private ObjectMapper objectMapper;
    private File jsonFile;

    public JacksonParser(File jsonFile)
    {
        this.jsonFile = jsonFile;
        objectMapper = new ObjectMapper();
    }

    public List<Candy> getCandyList() {



        Candy[] candies = new Candy[0];
        try {
            candies = objectMapper.readValue(jsonFile, Candy[].class);
        } catch(Exception ex){
            ex.printStackTrace();
        }

        return Arrays.asList(candies);
    }




}

package json;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kurylchyk.Candy;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GsonParser {
    private GsonBuilder gsonBuilder = new GsonBuilder();
    private Gson gson;
    private File jsonFile;

   public  GsonParser(File filename){

       gson = gsonBuilder.create();
       jsonFile = filename;

    }

    public List<Candy> getCandyList(){

        Candy[] candies =new  Candy[0];

        try{
            candies = gson.fromJson(new FileReader(jsonFile),Candy[].class);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return Arrays.asList(candies);
    }

    public void fillJSON(List<Candy> candies) {

        List<Candy> candySorted = candies.stream()
                .sorted((o1,o2)-> o1.compareTo(o2))
                .collect(Collectors.toList());

       try(Writer writer = new FileWriter(jsonFile)){
            gson.toJson(candySorted,writer);
       } catch(Exception ex){
           ex.printStackTrace();
       }
    }
}

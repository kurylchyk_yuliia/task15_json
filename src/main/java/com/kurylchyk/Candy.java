package com.kurylchyk;

import java.util.Comparator;

public class Candy implements  Comparator<Candy>, Comparable<Candy> {
    private String name;
    private double energy;
    private String type;
    private int water;
    private int sugar;
    private int fructose;
    private int vanillin;

    private double fat;
    private double protein;
    private double carbohydrate;

    private String production;

    Candy() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public void setFructose(int fructose) {
        this.fructose = fructose;
    }

    public void setVanillin(int vanillin) {
        this.vanillin = vanillin;
    }


    public void setFat(double fat) {
        this.fat = fat;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public void setCarbohydrate(double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }


    public void setProduction(String production){
        this.production = production;
    }
    public String toString(){
        return "Name: "+ name+"\n" +
                "Energy: "+energy+"\n"+
                "Type: "+type +"\n"+
                "Ingredients: " + "water "+water+" sugar "+sugar + " fructose "+fructose + " vanillin "+vanillin +"\n"+
                "Value: "+"fat "+fat +" protein "+protein+" carbohydrate "+carbohydrate+"\n"+
                "Production: "+production+"\n";
    }


    @Override
    public int compare(Candy o1, Candy o2) {
        return o1.name.compareTo(o2.name);
    }

    @Override
    public int compareTo(Candy o) {
        return this.name.compareTo(o.name);
    }
}

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;
import com.kurylchyk.Candy;
import json.GsonParser;
import json.JacksonParser;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.*;
import validate.JSONValidator;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    private static File jsonFile = new File("src\\main\\resources\\Candies.json");
    private static File jsonSchema = new File("src\\main\\resources\\CandiesSchema.json");

    public static void main(String[] args) {

        try {
           boolean valid = JSONValidator.validate(jsonFile, jsonSchema);
           logger.warn("The validation is "+ (valid==true?"successful":"failed"));

        } catch (IOException | ProcessingException ex) {
            ex.printStackTrace();
        }

        logger.trace("USING JACKSON");
        JacksonParser jacksonParser = new JacksonParser(jsonFile);

        List<Candy> list = jacksonParser.getCandyList();

        for (Candy element : list) {
            logger.info(element);
        }


        logger.trace("USING GSON");

        GsonParser gsonParser = new GsonParser(jsonFile);

        List<Candy> listGson = gsonParser.getCandyList();

        for (Candy element : listGson) {
            logger.info(element);
        }

        gsonParser.fillJSON(listGson); // adding the json file ot json
    }
}
